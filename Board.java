import java.util.Scanner;


public class Board {

	/**
	 * Draw L-shape and rotate 0 degree (default L).
	 *  _
	 * | |
	 * | |__
	 * |____|
	 * 
	 * (xpos0, ypos0) is on top-left corner.
	 * (xpos1, ypos1) is on bottom-right corner.
	 * 
	 * @param xpos the position in x-axis.
	 * @param ypos the position in y-axis
	 * @param size of board (as an input)
	 */
	public static void drawL0(double xpos, double ypos, int size) {

		double xpos0 = xpos - 1.0/size;
		double ypos0 = ypos + 1.0/size;

		double xpos1 = xpos + 1.0/size;
		double ypos1 = ypos - 1.0/size;

		// draw left line

		StdDraw.line(xpos, ypos, xpos, ypos0);

		StdDraw.line(xpos, ypos0, xpos0, ypos0);

		// draw right line

		StdDraw.line(xpos, ypos, xpos1, ypos);

		StdDraw.line(xpos1, ypos, xpos1, ypos1);

		// connect left and right

		StdDraw.line(xpos0, ypos0, xpos0, ypos1);

		StdDraw.line(xpos1, ypos1, xpos0, ypos1);

	}

	/**
	 * Draw L-shape and rotate 90 degree.
	 *  
	 *  _______
	 * |  _____|       
	 * | |
	 * | |
	 * |_|
	 * 
	 * (xpos0, ypos0) is on bottom-left corner.
	 * (xpos1, ypos1) is on top-right corner.
	 * 
	 * @param xpos the position in x-axis.
	 * @param ypos the position in y-axis
	 * @param size of board (as an input)
	 */
	public static void drawL90(double xpos, double ypos, int size) {

		double xpos0 = xpos - 1.0/size;
		double ypos0 = ypos - 1.0/size;

		double xpos1 = xpos + 1.0/size;
		double ypos1 = ypos + 1.0/size;

		// draw left line

		StdDraw.line(xpos, ypos, xpos, ypos0);

		StdDraw.line(xpos, ypos0, xpos0, ypos0);

		// draw right line

		StdDraw.line(xpos, ypos, xpos1, ypos);

		StdDraw.line(xpos1, ypos, xpos1, ypos1);

		// connect left and right

		StdDraw.line(xpos0, ypos0, xpos0, ypos1);

		StdDraw.line(xpos1, ypos1, xpos0, ypos1);

	}

	/**
	 * Draw L-shape and rotate 180 degree.
	 *  _____
	 * |___  |
	 *     | |
	 *     |_|
	 * 
	 * (xpos0, ypos0) is on top-left corner.
	 * (xpos1, ypos1) is on bottom-right corner.
	 * 
	 * @param xpos the position in x-axis.
	 * @param ypos the position in y-axis
	 * @param size of board (as an input)
	 */
	public static void drawL180(double xpos, double ypos, int size) {

		double xpos0 = xpos - 1.0/size;
		double ypos0 = ypos + 1.0/size;

		double xpos1 = xpos + 1.0/size;
		double ypos1 = ypos - 1.0/size;

		// draw left line

		StdDraw.line(xpos, ypos, xpos0, ypos);

		StdDraw.line(xpos0, ypos, xpos0, ypos0);

		// draw right line

		StdDraw.line(xpos, ypos, xpos, ypos1);

		StdDraw.line(xpos, ypos1, xpos1, ypos1);

		// connect left and right

		StdDraw.line(xpos0, ypos0, xpos1, ypos0);

		StdDraw.line(xpos1, ypos1, xpos1, ypos0);

	}

	/**
	 * Draw L-shape and rotate 270 degree.
	 *     __
	 *    |  |
	 *  __|  |
	 * |_____|
	 * 
	 * (xpos0, ypos0) is on bottom-left corner.
	 * (xpos1, ypos1) is on top-right corner.
	 * 
	 * @param xpos the position in x-axis.
	 * @param ypos the position in y-axis
	 * @param size of board (as an input)
	 */
	public static void drawL270(double xpos, double ypos, int size) {

		double xpos0 = xpos - 1.0/size;
		double ypos0 = ypos - 1.0/size;

		double xpos1 = xpos + 1.0/size;
		double ypos1 = ypos + 1.0/size;

		// draw left line

		StdDraw.line(xpos, ypos, xpos0, ypos);

		StdDraw.line(xpos0, ypos, xpos0, ypos0);

		// draw right line

		StdDraw.line(xpos, ypos, xpos, ypos1);

		StdDraw.line(xpos, ypos1, xpos1, ypos1);

		// connect left and right

		StdDraw.line(xpos0, ypos0, xpos1, ypos0);

		StdDraw.line(xpos1, ypos1, xpos1, ypos0);

	}
	
	/**
	 * Drawing hole by randomizing location.
	 * @param xpos the position in x-axis.
	 * @param ypos the position in y-axis.
	 * @param input
	 */
	public static void drawHole(double xpos, double ypos, double size) {

		StdDraw.filledSquare(xpos, ypos, 1.0/(2*size));
	}

	/**
	 * Finding quadrant of new-recursed hole by comparing new-recursed hole and input hole. 
	 * 
	 * This is used in order to draw L-shape according to quadrant.
	 * 
	 * @param xpos the position of new-recursed hole in x-axis. 
	 * @param ypos the position of new-recursed hole in y-axis.
	 * @param xhole the position of default hole in x-axis. 
	 * @param yhole the position of default hole in y-axis.
	 * 
	 * @return quadrant 1, 2, 3 or 4.
	 */
	public static int findQuadrant(double xpos,double ypos , double xhole, double yhole) {

		if(xhole > xpos && yhole > ypos) return 1;

		else if(xhole < xpos && yhole > ypos) return 2;

		else if(xhole < xpos && yhole < ypos) return 3;

		else return 4;
	}

	/**
	 * This is the recursion method. it processes in step-by-step:
	 * 
	 * 1.) At first, the program finds the quadrant of the hole in actual way(comparing hole
	 * location to the center(0.5, 0.5)).
	 * 
	 * ps. We convert size from n to 0-1 coordinates. Therefore, we consider everything in 0-1.
	 * 
	 * 2.) Then program starts tiling (drawing L-shape) from the center until the border of the 
	 * opposition of the hole quadrant (note that program tiles for many times depending on size.)
	 * 
	 * Note that program draws L-shape in different ways depending on the quadrant.
	 * 
	 * holeQuadrant | L-shape
	 * 		1		|	  0
	 * 		2		|   270
	 * 		3       |   180
	 * 		4       |    90
	 * 
	 * 
	 * 3.) Each time tiling the board, mark the new hole for the latest tile (you will see that
	 * L-shape is the 3/4 of square, then we will mark on the corner which is actually the space.)
	 * 
	 * Change the default hole to the latest hole which is tiled (we will called old hole), 
	 * this leads to the step 4.
	 *  _   
	 * | |   * <-- mark here.
	 * | |__
	 * |____|
	 * 
	 * 
	 * 4.) Then we recurses the method by calling tile functions. The reason is we need to tile
	 * all around the new hole (in other words, tile for all quadrants except the opposite quadrant
	 * because it ruins the tiling) so no odd spaces occurs (every single tile is bounded).
	 * 
	 * 
	 * 5.) Program will check the new quadrant (comparing new hole to the old one) and tile 
	 * the board recursively until it finishes.
	 * 
	 * FAQ: Why do we have to compare new hole to the latest one which is changed instead of
	 * new hole to the default hole?
	 * 
	 * The answer is that otherwise the process is ruined. The default hole doesn't change location.
	 * Then the step 1-3 is still correct. However, in recursion step that tiling around the new hole is wrong
	 * because it still compares to the same location. Therefore, the result is going to be wrong.
	 * 
	 * 
	 * @param xpos the location of hole location in x-axis.
	 * @param ypos the location of hole location in y-axis.
	 * @param size input
	 * @param holeQuadrant the quadrant of hole using findQuadrant method.
	 * @param multiplier is used for shifting xpos, ypos for half size each time.
	 * @param holeX the location of default hole in x-axis.
	 * @param holeY the location of default hole in y-axis.
	 */
	public static void tile(double xpos, double ypos, int size, int holeQuadrant, int multiplier, double holeX, double holeY) {

		if(multiplier == 0) return; // base case: tile until it reaches the board (we tile for log2(multiplier) times).

		multiplier /= 2; // shifting x, y for half size.

		holeQuadrant = findQuadrant(xpos,ypos,holeX,holeY); // find the quadrant of hole.
		
		if(holeQuadrant == 1) {

			drawL0(xpos, ypos, size);

			tile(xpos - (multiplier * (1.0/(size))), ypos - (multiplier*(1.0/(size))), size, 1, multiplier, xpos + size/2, ypos + size/2);

			tile(xpos + (multiplier * (1.0/(size))), ypos - (multiplier*(1.0/(size))), size, 2, multiplier, xpos - size/2, ypos + size/2);

			tile(xpos - (multiplier * (1.0/(size))), ypos + (multiplier * (1.0/(size))), size, 4, multiplier, xpos + size/2, ypos - size/2);

			tile(xpos + (multiplier * (1.0/(size))), ypos + (multiplier * (1.0/(size))), size, 1, multiplier, holeX, holeY);
		}

		if(holeQuadrant == 2) {

			drawL270(xpos, ypos, size);

			tile(xpos + (multiplier * (1.0/(size))), ypos - (multiplier*(1.0/(size))), size, 2, multiplier,xpos-size/2,ypos + size/2);

			tile(xpos - (multiplier * (1.0/(size))), ypos - multiplier*(1.0/(size)), size, 1, multiplier,xpos+size/2,ypos+size/2);

			tile(xpos + (multiplier * (1.0/(size))), ypos + multiplier*(1.0/(size)), size, 3, multiplier,xpos-size/2,ypos-size/2);

			tile(xpos - multiplier * (1.0/(size)), ypos + multiplier * (1.0/(size)), size, 2, multiplier,holeX,holeY);
		}

		if(holeQuadrant == 3) {

			drawL180(xpos, ypos, size);

			tile(xpos + (multiplier * (1.0/(size))), ypos + multiplier*(1.0/(size)), size, 3, multiplier,xpos-size/2,ypos-size/2);

			tile((xpos + multiplier*(1.0/size)), (ypos - multiplier * (1.0/(size))), size, 2, multiplier,xpos-size/2,ypos+size/2);

			tile((xpos - multiplier*(1.0/size)), (ypos + multiplier*(1.0/(size))), size, 4, multiplier,xpos+size/2,ypos-size/2);

			tile(xpos- multiplier*(1.0/size) , ypos- multiplier*(1.0/size), size, 3, multiplier,holeX,holeY);
		}

		else if(holeQuadrant == 4) {

			drawL90(xpos, ypos, size);

			tile(xpos - (multiplier * (1.0/(size))), ypos + (multiplier * (1.0/(size))), size, 4, multiplier,xpos+size/2,ypos-size/2);

			tile(xpos - (multiplier * (1.0/(size))), ypos - multiplier*(1.0/(size)), size, 1, multiplier,xpos+size/2,ypos+size/2);

			tile(xpos + (multiplier * (1.0/(size))), ypos + multiplier*(1.0/(size)), size, 3, multiplier,xpos-size/2,ypos-size/2);

			tile(xpos + multiplier * (1.0/(size)), ypos - multiplier * (1.0/(size)), size, 4, multiplier,holeX,holeY);
		}
	}

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		System.out.print("Input board size (n x n) : ");

		int n = scanner.nextInt(); // input size

		double size = 1.0/(n); // convert to 0-1 coordinates.

		int x = (int)(Math.random()*n) + 1; // random x location.

		int y = (int)(Math.random()*n) + 1; // random y location.

		double holeX = (x*size) - (size/2); // this formula is used for avoiding the odd location (hole is overlaid line).
		
		double holeY = (y*size) - (size/2); // this formula is used for avoiding the odd location (hole is overlaid line).
		
		drawHole(holeX, holeY, n);

		int holeQuadrant = findQuadrant(0.5,0.5,holeX,holeY);

		int times = n/2;

		tile(0.5, 0.5, n, holeQuadrant, times,holeX,holeY);

	}
}
