
public class ListUtil {

	/*
	Given a list and an int, return the number of times that int occurs in the
	list.
	 */
	public static int Count(ListCell<Integer> head, int searchFor) {

		ListCell<Integer> currentNode = head;

		if(currentNode == null) return 0;

		if((int)currentNode.getDatum() == searchFor) {

			return 1 + Count(currentNode.getNext(), searchFor);
		}

		return Count(head.getNext(), searchFor);

	}

	/*
	Given a list, an index 'n' in the range 0..length, and a data element, add a
	new node to the list so that it has the given index.
	 */
	public static ListCell<Integer> InsertNth(ListCell<Integer> headRef, int index, int data) {

		ListCell<Integer> currentNode = headRef;

		ListCell<Integer> newCell = new ListCell<Integer>(data, null);

		if(index == 0) {	
			newCell.setNext(currentNode);
			return newCell;
		}

		currentNode.setNext(InsertNth(currentNode.getNext(), index - 1, data));

		return currentNode;


	}

	public static ListCell<Integer> SortedInsert(ListCell<Integer> headRef, ListCell<Integer> newNode) {

		ListCell<Integer> currentNode = headRef;

		if(currentNode == null) {

			return newNode;

		}

		if(newNode.getDatum() <= currentNode.getDatum()) {

			newNode.setNext(currentNode);

			return newNode;

		}

		currentNode.setNext(SortedInsert(currentNode.getNext(), newNode));

		return currentNode;
	}

	// Given a list, change it to be in sorted order (using SortedInsert()). 

	public static ListCell<Integer> InsertSort(ListCell<Integer> headRef) {

		ListCell<Integer> sortedCell = new ListCell<Integer>(headRef.getDatum(), null);

		ListCell<Integer> currentCell = headRef.getNext();

		while(currentCell != null) {

			ListCell<Integer> temp = new ListCell<Integer>(currentCell.getDatum(), null);

			sortedCell = SortedInsert(sortedCell, temp);

			currentCell = currentCell.getNext();

		}

		return sortedCell;
	}

	/*
	Remove duplicates from a sorted list.
	 */
	public static ListCell<Integer> RemoveDuplicates(ListCell<Integer> head) {

//		ListCell<Integer> currentCell = head;
//
//		ListCell<Integer> nextCell = currentCell.getNext();
//
//		while(nextCell != null) {
//
//			if(currentCell.getDatum() == nextCell.getDatum()) {
//
//				currentCell.setNext(nextCell.getNext());
//
//			}
//
//			currentCell = currentCell.getNext();
//
//			nextCell = nextCell.getNext();
//
//		}
//
//		return head;
		
		ListCell<Integer> currentCell = head;

		if(currentCell.getNext() == null) return currentCell;
		
		if(currentCell.getDatum() == RemoveDuplicates(currentCell.getNext()).getDatum()) {
			
			return RemoveDuplicates(currentCell.getNext());
		}
		
		currentCell.setNext(RemoveDuplicates(currentCell.getNext()));
		
		return currentCell;
	}

	public static String displayCell(ListCell<Integer> head) {

		String output = "";

		while(head != null) {
			output += String.format("[%d]", head.getDatum());
			head = head.getNext();
		}

		return output;

	}

	public static void main(String[] args) {

		System.out.println();

		ListCell<Integer> cell_1;
		ListCell<Integer> cell_2;
		ListCell<Integer> cell_3;
		ListCell<Integer> cell_4;
		ListCell<Integer> cell_5;

		System.out.println("---------- Count ----------");
		System.out.println();
		System.out.println("Example: We need to count 3 in ListCell...");
		System.out.println();

		cell_1 = new ListCell<Integer>(1, null);
		cell_2 = new ListCell<Integer>(2, null);
		cell_3 = new ListCell<Integer>(3, null);
		cell_4 = new ListCell<Integer>(4, null);
		cell_5 = new ListCell<Integer>(5, null);

		cell_1.setNext(cell_2);
		cell_2.setNext(cell_3);
		cell_3.setNext(cell_4);
		cell_4.setNext(cell_5);

		System.out.printf("%s -> %d\n", displayCell(cell_1), Count(cell_1, 3));

		System.out.println();

		cell_4.setDatum(3);
		cell_5.setDatum(3);

		System.out.printf("%s -> %d\n", displayCell(cell_1), Count(cell_1, 3));

		System.out.println();

		cell_3.setDatum(6);
		cell_4.setDatum(6);
		cell_5.setDatum(6);

		System.out.printf("%s -> %d\n", displayCell(cell_1), Count(cell_1, 3));

		System.out.println();

		System.out.println("---------- InsertNth ----------");

		System.out.println();

		System.out.println("Example: We need to add 6 in the different indexes.");

		System.out.println();

		cell_3.setDatum(3);
		cell_4.setDatum(4);
		cell_5.setDatum(5);

		System.out.println("\tIndex\t | \t\tBefore\t\t | \t\tAfter\t");

		System.out.println();

		System.out.printf("\t%3d\t | \t%18s\t | \t%s\t\n\n", 0, displayCell(cell_1), displayCell(InsertNth(cell_1, 0, 6)));

		System.out.printf("\t%3d\t | \t%18s\t | \t%s\t\n\n", 5, displayCell(cell_1), displayCell(InsertNth(cell_1, 5, 6)));

		System.out.printf("\t%3d\t | \t%18s\t | \t%s\t\n\n", 2, displayCell(cell_1), displayCell(InsertNth(cell_1, 2, 6)));

		System.out.println("---------- SortedInsert ----------");

		System.out.println();

		cell_1.setNext(cell_2);
		cell_2.setNext(cell_3);
		cell_3.setNext(cell_4);
		cell_4.setNext(cell_5);
		cell_5.setNext(null);

		System.out.println("Example: Insert different nodes into the existing list.");

		System.out.println();

		System.out.println("\tValue\t | \t\tBefore\t\t | \t\tAfter\t");

		System.out.println();

		ListCell<Integer> temp = new ListCell<Integer>(6, null);

		System.out.printf("\t%3d\t | \t%18s\t | \t%s\t\n\n", 6, displayCell(cell_1), displayCell(SortedInsert(cell_1, temp)));

		cell_1.setNext(cell_2);
		cell_2.setNext(cell_3);
		cell_3.setNext(cell_4);
		cell_4.setNext(cell_5);
		cell_5.setNext(null);

		temp.setDatum(2);

		System.out.printf("\t%3d\t | \t%18s\t | \t%s\t\n\n", 2, displayCell(cell_1), displayCell(SortedInsert(cell_1, temp)));

		cell_1.setNext(cell_2);
		cell_2.setNext(cell_3);
		cell_3.setNext(cell_4);
		cell_4.setNext(cell_5);
		cell_5.setNext(null);

		temp.setDatum(0);

		System.out.printf("\t%3d\t | \t%18s\t | \t%s\t\n\n", 0, displayCell(cell_1), displayCell(SortedInsert(cell_1, temp)));

		System.out.println("---------- InsertSort ----------");

		System.out.println();

		System.out.println("Example: Sort the different lists.");

		System.out.println();

		System.out.println("\t\tBefore\t\t | \t\tAfter\t");

		System.out.println();

		cell_1.setDatum(5);
		cell_2.setDatum(4);
		cell_3.setDatum(3);
		cell_4.setDatum(2);
		cell_5.setDatum(1);

		System.out.printf(" \t%18s\t | \t%18s\t\n\n", displayCell(cell_1), displayCell(InsertSort(cell_1)));

		cell_1.setDatum(6);
		cell_2.setDatum(3);
		cell_3.setDatum(2);
		cell_4.setDatum(8);
		cell_5.setDatum(9);

		System.out.printf(" \t%18s\t | \t%18s\t\n\n", displayCell(cell_1), displayCell(InsertSort(cell_1)));

		cell_1.setDatum(9);
		cell_2.setDatum(9);
		cell_3.setDatum(9);
		cell_4.setDatum(9);
		cell_5.setDatum(9);

		System.out.printf(" \t%18s\t | \t%18s\t\n\n", displayCell(cell_1), displayCell(InsertSort(cell_1)));

		System.out.println("---------- RemoveDuplicates ----------");

		System.out.println();

		System.out.println("Example: Remove duplicated cell from the different lists.");

		System.out.println();
		
		System.out.println("\t\tBefore\t\t | \t\tAfter\t");

		System.out.println();

		cell_1.setDatum(1);
		cell_2.setDatum(2);
		cell_3.setDatum(2);
		cell_4.setDatum(3);
		cell_5.setDatum(4);
		
		System.out.printf(" \t%18s\t | \t%18s\t\n\n", displayCell(cell_1), displayCell(RemoveDuplicates(cell_1)));
		
		cell_1.setNext(cell_2);
		cell_2.setNext(cell_3);
		cell_3.setNext(cell_4);
		cell_4.setNext(cell_5);
		cell_5.setNext(null);
		
		cell_1.setDatum(6);
		cell_2.setDatum(6);
		cell_3.setDatum(9);
		cell_4.setDatum(9);
		cell_5.setDatum(9);
		
		System.out.printf(" \t%18s\t | \t%18s\t\n\n", displayCell(cell_1), displayCell(RemoveDuplicates(cell_1)));
		
		cell_1.setNext(cell_2);
		cell_2.setNext(cell_3);
		cell_3.setNext(cell_4);
		cell_4.setNext(cell_5);
		cell_5.setNext(null);
		
		cell_1.setDatum(1);
		cell_2.setDatum(2);
		cell_3.setDatum(3);
		cell_4.setDatum(4);
		cell_5.setDatum(5);
		
		System.out.printf(" \t%18s\t | \t%18s\t\n\n", displayCell(cell_1), displayCell(RemoveDuplicates(cell_1)));
	}
}
